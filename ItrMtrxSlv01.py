# load library
import numpy as np
import time as tm

# give the entries in matrix A and column vector f
A = np.array([[-3, 1, 2],[-1, -3, 1],[-1, 3, 4]])  

f = np.array([2, 6, 4])

# decompose A to D and R matrices
D = np.zeros((3,3))
R = np.zeros((3,3))
for n in range(3):
    for m in range(3):
        if n == m:
            D[n,m] = A[n,m]
        else: 
            R[n,m] = A[n,m]


# inverse of D matrix
Dinv = np.zeros((3,3))
for n in range(3):
    Dinv[n,n] = 1.0/D[n,n]
    

# initial value of x
x = np.array([0, 0, 0])

# iteration
cpu_tm_bg = tm.time()
for iter in range(10):
    
    tmp = f - np.matmul(R,x)
    x = np.matmul(Dinv,tmp)
    
    resd = f - np.matmul(A,x)
    nrm = 0
    for n in range(len(resd))
        nrm=nrm + (resd[n]**2)
    nrm=nrm**0.5
    print(nrm)

print(np.matmul(A,x))
# walltime before matrix solving
cpu_tm_ed = tm.time()
print(cpu_tm_ed-cpu_tm_bg)
# iteration
    # use function np.matmul for matrix-vector multiplication
 

    # residual during iterations

    
    # use calculate the norm of the residual using numpy function

    


    

# walltime after matrix solving
cpu_tm_ed = tm.time()    

# print results
print(cpu_tm_ed - cpu_tm_bg, nrm)

